import json
import pandas
from os import listdir
from os.path import isfile, join


df = pandas.read_csv('data/urllist.csv')
df['file'] = df['url'].apply(lambda x: x.split('/')[-1])
df['text'] = ""

for dir in ["data/annotation/annotation%d"%i for i in range(12)]:
    for file in [f for f in listdir(dir) if isfile(join(dir, f))]:
        print(join(dir, file))
        f = open(join(dir, file)).read()
        data = json.loads(f)
        for item in data['responses']:
            if 'textAnnotations' in item.keys():
                uri = item['context']['uri']
                uri = uri.split('/')[-1]
                text = item['textAnnotations'][0]['description']
                df.loc[df['file']==uri, "text"] = text

df.to_csv('data/annotatedurl.csv', index=None)
