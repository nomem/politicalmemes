import pandas
from google.cloud import vision_v1
from google.cloud.vision_v1 import enums


def sample_async_batch_annotate_images(image_uris=[], output_uri=""):
    """Perform async batch image annotation."""
    client = vision_v1.ImageAnnotatorClient()

    features = [
        {"type": enums.Feature.Type.TEXT_DETECTION},
    ]
    requests = [{"image": {"source": {"image_uri": uri}}, "features": features} for uri in image_uris]
    gcs_destination = {"uri": output_uri}

    # The max number of responses to output in each JSON file
    batch_size = 100
    output_config = {"gcs_destination": gcs_destination, "batch_size": batch_size}

    operation = client.async_batch_annotate_images(requests, output_config)

    print("Waiting for operation to complete...")
    response = operation.result()

    # The output is written to GCS with the provided output_uri as prefix
    gcs_output_uri = response.output_config.gcs_destination.uri
    print("Output written to GCS with prefix: {}".format(gcs_output_uri))


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def main():
    df = pandas.read_csv('data/urllist.csv')
    urls = df['url'].tolist()
    urls = [url.split('/')[-1] for url in urls]
    cnt = 0
    for chunk in chunks(urls, 2000):
        image_uris = ["gs://polmeme/images/%s"%url for url in chunk]
        output_uri="gs://polmeme/annotation%d/" % cnt
        sample_async_batch_annotate_images(image_uris=image_uris, output_uri=output_uri)
        cnt += 1


if __name__ == '__main__':
    main()
    