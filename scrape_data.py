import requests
import pandas
import time

subreddits = ['neoliberal', 'chapotraphouse', 'the_donald']
date_range = [40, 100, 1]
api_url = 'https://api.pushshift.io/reddit/search/submission/?subreddit=%s&fields=url&size=500&after=%dd&before=%dd'


def getData(URL):
    r = requests.get(url = URL) 
    data = r.json()
    return [dic['url'] for dic in data['data']]


def main():
    sub = []
    url = []
    for subreddit in subreddits:
        for before_date in range(date_range[0], date_range[1], date_range[2]):
            time.sleep(1)
            URL = api_url % (subreddit, before_date+date_range[2], before_date)
            urls = getData(URL)
            print("sub:%s day: %d count: %d"% (subreddit, before_date, len(urls)))
            for urlt in urls:
                if 'i.redd.it' in urlt and ('.jpg' in urlt or '.png' in urlt):
                    url.append(urlt)
                    sub.append(subreddit)
    df = pandas.DataFrame({'sub':sub, 'url': url})
    df.to_csv('data/urllist.csv', index=None)

if __name__ == '__main__':
    main()
    